# Block[Chain] Alchemy Laboratory Framework

The goal of the project "Block[Chain] Alchemy Lab" ‒ creation of a toolkit for educational and research activities for blockchain-related tasks. This toolkit consist from programming framework and tools for modelling different blockchain and not only blockchain (indicated by the optionality of "chain") environments. Programming framework oriented to flexibility and possibility of changing different layers of architecture: consensus machinery, storage systems, transaction transport, smart contract implementation, etc. 

## Installation

1. Make sure [Python](https://www.python.org/downloads/) 2 or 3 is installed. 


2. Install requirements (preferably inside a [virtual environment](https://docs.python.org/3/tutorial/venv.html)).  

```
$ pip install -r requirements.txt
``` 

## Run the server:
    * `$ python2 bal/blockchain.py -p 5001 -d pow.db -v pow:2`
    * `$ python3 bal/blockchain.py --port 5002 --db quant.db --variant quant`
    
### Parameters

    '-p', '--port', default=5000, type=int, help='port to listen on'
    '-d', '--db', default='', help='db file, if not passed, then no persistance'
    '-v', '--variant', default='pow', help='variant of blockchain "pow[:difficulty]" or "quant", where:
          * pow[:difficulty] -- POWBlockChain with possibility of "difficulty" setting (4 by default)
          * quant -- QuantumBlockChain

For QuantumBlockChain, the [keyworker](https://gitlab.com/BAlchemyLab/qnet/tree/master/keyworker) service must be started.

## Testing
Internal test:
```
python -m unittest tests.test_BaseBlockChain
```

Test REST API POW:
1. Run blockchain.py servers on ports 5000 and 5001 with POW variant (`--variant pow`):
`python bal/blockchain.py --port 5000 --variant pow` and `python bal/blockchain.py --port 5001 --variant pow`
in separate terminals.
2. Run test script:
```bash
./tests/test.sh
```
More details in [testpow.md](docs/testpow.md).

Test REST API QUANTUM:
1. Run blockchain.py servers on ports 5000 and 5001 with quantum variants (`--variant quant`):
`python bal/blockchain.py --port 5000 --variant quant` and `python bal/blockchain.py --port 5001 --variant quant`
2. Run [keyworker](https://gitlab.com/BAlchemyLab/qnet/tree/master/keyworker) on port 55554:
`keyworker -p 55554`  
2. Run test script:
```
./tests/testquantum.sh
```

More details in [testquantum.md](docs/testquantum.md).

## Blockchain Mininet network
Mininet's custom setup for BAL classes balmn.py add several types of hosts:
* btc -- bcnode.BtcNode
* eth -- bcnode.EthNode
* pow -- bcnode.POWNode
* qkd -- bcnode.QNode

For example:
```
sudo mn --custom=bal/balmn.py --host=btc
```

Testing:
```
sudo ./bal/bcmn_test.py [host_type]
```


**Test with quantum links:**
- start keyworker in the separate terminal: `keyworker -p 55554` 
- start topology `sudo mn --custom=bal/QKCustom.py --link=qk --topo=tree,depth=2,fanout=3`

With `--link=qk` you will have quantum encoded data between nodes. You can try to run `pingall` in mininet console
to check connectivity.

Please note, that this topology requires tools from https://gitlab.com/BAlchemyLab/qnet installer.

**Test with ryu:**

```bash
cd bal
sudo mn --custom bal/balmn.py --topo qtopo --mac --controller remote --switch ovs
```

Go to 'bal' directory and start ryu-manager:

```bash
cd bal
ryu-manager rest_forward.py
```

You can switch channels with RestAPI:
For raw channel:
```bash
curl -X GET http://localhost:8080/channel/1/1
```
For quantum channel:
```bash
curl -X GET http://localhost:8080/channel/1/2
```

## More information
you may find in the project's [Wiki](https://gitlab.com/BAlchemyLab/bal/wikis/home).
