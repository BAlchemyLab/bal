# Test REST API QUANTUM

This test uses quantum variant of blockchain. It registers nodes, mines new block,
does consensus and checks new chain. In this case we use special tool called `keyworker` to create
new block and check it from other nodes.

```console
curl -s -X POST -H Content-Type: application/json -d { "keyworkers": [{"address":"http://localhost:55554","id":0}] } http://localhost:5000/keyworker/register
{"message":"New keyworkers have been added","total_nodes":[{"address":"localhost:55554","device_num":-1,"id":0}]}
curl -s -X POST -H Content-Type: application/json -d { "keyworkers": [{"address":"http://localhost:55554","id":0}] } http://localhost:5001/keyworker/register
{"message":"New keyworkers have been added","total_nodes":[{"address":"localhost:55554","device_num":-1,"id":0}]}
curl http://localhost:5000/nodes
{"message":"Nodes","total_nodes":[]}
curl -s -X POST -H Content-Type: application/json -d { "nodes": [{"address":"http://localhost:5001","keyworker_id":0}] } http://localhost:5000/nodes/register
{"message":"New nodes have been added","total_nodes":[{"address":"localhost:5001","keyworker_id":0}]}
curl http://localhost:5000/nodes
{"message":"Nodes","total_nodes":[{"address":"localhost:5001","keyworker_id":0}]}
curl -s -X DELETE -H Content-Type: application/json http://localhost:5000/nodes
{"message":"Nodes removed","total_nodes":[]}
curl http://localhost:5000/nodes
{"message":"Nodes","total_nodes":[]}
curl -s -X POST -H Content-Type: application/json -d { "nodes": [{"address":"http://localhost:5001","keyworker_id":0}] } http://localhost:5000/nodes/register
{"message":"New nodes have been added","total_nodes":[{"address":"localhost:5001","keyworker_id":0}]}
curl http://localhost:5000/nodes
{"message":"Nodes","total_nodes":[{"address":"localhost:5001","keyworker_id":0}]}
ADDR0={"index":2,"message":"New Block Forged","previous_hash":"1cad547813744e1a22f477eceb3d5adf46bf8bb2e3dcf2887b3e421ac01f01fd","proof":"7a34f7e5c62b544870140cf9531af363f9f9f2c7876cd3dab95daefe7a24ccac","transactions":[{"amount":1,"recipient":"e8a6d1f8b17b46b5aa56b15541c1c7f1","sender":"0"}]}
ADDR1={"index":2,"message":"New Block Forged","previous_hash":"bffd526453b0b3d04e8f39f773c09084bae18c71c4b6cf20d249a4abf467feb3","proof":"d97e969e67d579d3f15ecc96dfc0e9139750694d0451d332a6ad3692f4817503","transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]}

Sending 5 from ADDR0 to ADDR1:

curl -s -X POST -H Content-Type: application/json -d { "sender": "{"index":2,"message":"New Block Forged","previous_hash":"1cad547813744e1a22f477eceb3d5adf46bf8bb2e3dcf2887b3e421ac01f01fd","proof":"7a34f7e5c62b544870140cf9531af363f9f9f2c7876cd3dab95daefe7a24ccac","transactions":[{"amount":1,"recipient":"e8a6d1f8b17b46b5aa56b15541c1c7f1","sender":"0"}]}", "recipient": "{"index":2,"message":"New Block Forged","previous_hash":"bffd526453b0b3d04e8f39f773c09084bae18c71c4b6cf20d249a4abf467feb3","proof":"d97e969e67d579d3f15ecc96dfc0e9139750694d0451d332a6ad3692f4817503","transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]}", "amount": 5 } http://localhost:5001/transactions/new

curl -s -X GET -H Content-Type: application/json http://localhost:5000/mine
{"index":3,"message":"New Block Forged","previous_hash":"80240282266fe1bd2b70374d84d14f6b8c8e7aef79a4d334b942fb09a73ef841","proof":"f1164fb76adc53ac4bd22004ff616b4580a985e3cd127afb3e7b9598df44396d","transactions":[{"amount":1,"recipient":"e8a6d1f8b17b46b5aa56b15541c1c7f1","sender":"0"}]}

curl -s -X GET -H Content-Type: application/json http://localhost:5000/chain
{"chain":[{"index":1,"previous_hash":"1","proof":100,"timestamp":1628094004.885999,"transactions":[]},{"index":2,"previous_hash":"1cad547813744e1a22f477eceb3d5adf46bf8bb2e3dcf2887b3e421ac01f01fd","proof":"7a34f7e5c62b544870140cf9531af363f9f9f2c7876cd3dab95daefe7a24ccac","timestamp":1628094032.0332532,"transactions":[{"amount":1,"recipient":"e8a6d1f8b17b46b5aa56b15541c1c7f1","sender":"0"}]},{"index":3,"previous_hash":"80240282266fe1bd2b70374d84d14f6b8c8e7aef79a4d334b942fb09a73ef841","proof":"f1164fb76adc53ac4bd22004ff616b4580a985e3cd127afb3e7b9598df44396d","timestamp":1628094032.0503063,"transactions":[{"amount":1,"recipient":"e8a6d1f8b17b46b5aa56b15541c1c7f1","sender":"0"}]}],"length":3}

Sending 10 from ADDR1 to ADDR0:

curl -s -X POST -H Content-Type: application/json -d { "sender": "{"index":2,"message":"New Block Forged","previous_hash":"bffd526453b0b3d04e8f39f773c09084bae18c71c4b6cf20d249a4abf467feb3","proof":"d97e969e67d579d3f15ecc96dfc0e9139750694d0451d332a6ad3692f4817503","transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]}", "recipient": "{"index":2,"message":"New Block Forged","previous_hash":"1cad547813744e1a22f477eceb3d5adf46bf8bb2e3dcf2887b3e421ac01f01fd","proof":"7a34f7e5c62b544870140cf9531af363f9f9f2c7876cd3dab95daefe7a24ccac","transactions":[{"amount":1,"recipient":"e8a6d1f8b17b46b5aa56b15541c1c7f1","sender":"0"}]}", "amount": 10 } http://localhost:5001/transactions/new

curl -s -X GET -H Content-Type: application/json http://localhost:5001/mine
{"index":3,"message":"New Block Forged","previous_hash":"73f33e671eb1bdf22040e994f9d992197abf513f058d6e060525f37a495f1da2","proof":"306c0b6229164236c3447277eb51d6d0affb439d1d94cc419cbfb537094d2364","transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]}

curl -s -X GET -H Content-Type: application/json http://localhost:5001/mine
{"index":4,"message":"New Block Forged","previous_hash":"c988bf7c657220cf75d1ef06f3d92ef04a4edb36dc827291f7f01141c8474d5b","proof":"93d3253ddc305b1165a1a0ff68b97694700a32db1c986caa34e3d6d18fef8b18","transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]}

curl -s -X GET -H Content-Type: application/json http://localhost:5001/mine
{"index":5,"message":"New Block Forged","previous_hash":"8b89527a1282b5f29f4ea9f7e053aed8267cdf34f4eb9d49d2f47b9ec1af4aac","proof":"f47532ba8f6d39d0d009bae1e12bf35a64d3d050f26e68b9c5a56d0a6fdfd706","transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]}

curl -s -X GET -H Content-Type: application/json http://localhost:5000/nodes/resolve
{"message":"Our chain was replaced","new_chain":[{"index":1,"previous_hash":"1","proof":100,"timestamp":1628094013.0035412,"transactions":[]},{"index":2,"previous_hash":"bffd526453b0b3d04e8f39f773c09084bae18c71c4b6cf20d249a4abf467feb3","proof":"d97e969e67d579d3f15ecc96dfc0e9139750694d0451d332a6ad3692f4817503","timestamp":1628094032.0390606,"transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]},{"index":3,"previous_hash":"73f33e671eb1bdf22040e994f9d992197abf513f058d6e060525f37a495f1da2","proof":"306c0b6229164236c3447277eb51d6d0affb439d1d94cc419cbfb537094d2364","timestamp":1628094032.0665874,"transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]},{"index":4,"previous_hash":"c988bf7c657220cf75d1ef06f3d92ef04a4edb36dc827291f7f01141c8474d5b","proof":"93d3253ddc305b1165a1a0ff68b97694700a32db1c986caa34e3d6d18fef8b18","timestamp":1628094032.0721142,"transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]},{"index":5,"previous_hash":"8b89527a1282b5f29f4ea9f7e053aed8267cdf34f4eb9d49d2f47b9ec1af4aac","proof":"f47532ba8f6d39d0d009bae1e12bf35a64d3d050f26e68b9c5a56d0a6fdfd706","timestamp":1628094032.0775511,"transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]}]}

curl -s -X GET -H Content-Type: application/json http://localhost:5000/chain
{"chain":[{"index":1,"previous_hash":"1","proof":100,"timestamp":1628094013.0035412,"transactions":[]},{"index":2,"previous_hash":"bffd526453b0b3d04e8f39f773c09084bae18c71c4b6cf20d249a4abf467feb3","proof":"d97e969e67d579d3f15ecc96dfc0e9139750694d0451d332a6ad3692f4817503","timestamp":1628094032.0390606,"transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]},{"index":3,"previous_hash":"73f33e671eb1bdf22040e994f9d992197abf513f058d6e060525f37a495f1da2","proof":"306c0b6229164236c3447277eb51d6d0affb439d1d94cc419cbfb537094d2364","timestamp":1628094032.0665874,"transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]},{"index":4,"previous_hash":"c988bf7c657220cf75d1ef06f3d92ef04a4edb36dc827291f7f01141c8474d5b","proof":"93d3253ddc305b1165a1a0ff68b97694700a32db1c986caa34e3d6d18fef8b18","timestamp":1628094032.0721142,"transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]},{"index":5,"previous_hash":"8b89527a1282b5f29f4ea9f7e053aed8267cdf34f4eb9d49d2f47b9ec1af4aac","proof":"f47532ba8f6d39d0d009bae1e12bf35a64d3d050f26e68b9c5a56d0a6fdfd706","timestamp":1628094032.0775511,"transactions":[{"amount":1,"recipient":"b896ef7a38a541da94c0e477c308332c","sender":"0"}]}],"length":5}

Test passed -- the chain was changed.
```
