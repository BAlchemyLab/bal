# Test REST API POW

This test uses proof of work variant of blockchain. It registers nodes, mines new block,
does consensus and checks new chain.

```console
curl http://localhost:5000/nodes
{"message":"Nodes","total_nodes":[]}
curl -s -X POST -H Content-Type: application/json -d { "nodes": ["http://localhost:5001"] } http://localhost:5000/nodes/register
{"message":"New nodes have been added","total_nodes":["localhost:5001"]}
curl http://localhost:5000/nodes
{"message":"Nodes","total_nodes":["localhost:5001"]}
curl -s -X DELETE -H Content-Type: application/json http://localhost:5000/nodes
{"message":"Nodes removed","total_nodes":[]}
curl http://localhost:5000/nodes
{"message":"Nodes","total_nodes":[]}
curl -s -X POST -H Content-Type: application/json -d { "nodes": ["http://localhost:5001"] } http://localhost:5000/nodes/register
{"message":"New nodes have been added","total_nodes":["localhost:5001"]}
curl http://localhost:5000/nodes
{"message":"Nodes","total_nodes":["localhost:5001"]}
ADDR0={"index":2,"message":"New Block Forged","previous_hash":"06f64209dbb37ced04941147dd7286e7032eb15a7c95c9884978cef11e00ae11","proof":72056,"transactions":[{"amount":1,"recipient":"1fc0eee2a3454d7b9c6ac2c14118c594","sender":"0"}]}
ADDR1={"index":2,"message":"New Block Forged","previous_hash":"5a976c5fa7492662bf099c5716887f7320bb37ddb012a75906185e8e1c771280","proof":46358,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]}

Sending 5 from ADDR0 to ADDR1:

curl -s -X POST -H Content-Type: application/json -d { "sender": "{"index":2,"message":"New Block Forged","previous_hash":"06f64209dbb37ced04941147dd7286e7032eb15a7c95c9884978cef11e00ae11","proof":72056,"transactions":[{"amount":1,"recipient":"1fc0eee2a3454d7b9c6ac2c14118c594","sender":"0"}]}", "recipient": "{"index":2,"message":"New Block Forged","previous_hash":"5a976c5fa7492662bf099c5716887f7320bb37ddb012a75906185e8e1c771280","proof":46358,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]}", "amount": 5 } http://localhost:5001/transactions/new

curl -s -X GET -H Content-Type: application/json http://localhost:5000/mine
{"index":3,"message":"New Block Forged","previous_hash":"a8c34e223fffd81bc41469bd905f5545a10886493395d27fbb5d3047c42775eb","proof":4048,"transactions":[{"amount":1,"recipient":"1fc0eee2a3454d7b9c6ac2c14118c594","sender":"0"}]}

curl -s -X GET -H Content-Type: application/json http://localhost:5000/chain
{"chain":[{"index":1,"previous_hash":"1","proof":100,"timestamp":1628093912.3680563,"transactions":[]},{"index":2,"previous_hash":"06f64209dbb37ced04941147dd7286e7032eb15a7c95c9884978cef11e00ae11","proof":72056,"timestamp":1628093943.7672591,"transactions":[{"amount":1,"recipient":"1fc0eee2a3454d7b9c6ac2c14118c594","sender":"0"}]},{"index":3,"previous_hash":"a8c34e223fffd81bc41469bd905f5545a10886493395d27fbb5d3047c42775eb","proof":4048,"timestamp":1628093943.8346436,"transactions":[{"amount":1,"recipient":"1fc0eee2a3454d7b9c6ac2c14118c594","sender":"0"}]}],"length":3}

Sending 10 from ADDR1 to ADDR0:

curl -s -X POST -H Content-Type: application/json -d { "sender": "{"index":2,"message":"New Block Forged","previous_hash":"5a976c5fa7492662bf099c5716887f7320bb37ddb012a75906185e8e1c771280","proof":46358,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]}", "recipient": "{"index":2,"message":"New Block Forged","previous_hash":"06f64209dbb37ced04941147dd7286e7032eb15a7c95c9884978cef11e00ae11","proof":72056,"transactions":[{"amount":1,"recipient":"1fc0eee2a3454d7b9c6ac2c14118c594","sender":"0"}]}", "amount": 10 } http://localhost:5001/transactions/new

curl -s -X GET -H Content-Type: application/json http://localhost:5001/mine
{"index":3,"message":"New Block Forged","previous_hash":"4b62614dfdc227227b1d4fde56805dfe745c8d18cb8bbc037b526b05f3877281","proof":95587,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]}

curl -s -X GET -H Content-Type: application/json http://localhost:5001/mine
{"index":4,"message":"New Block Forged","previous_hash":"e2e5f0b0c0da7e8a3479967a1587aee601b59c2b7634b772e9b44a9d71ecea7d","proof":140560,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]}

curl -s -X GET -H Content-Type: application/json http://localhost:5001/mine
{"index":5,"message":"New Block Forged","previous_hash":"753d498348a2c679a33c72827af7f6c24b9379bdeeb3126ed79c00464736e070","proof":1111,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]}

curl -s -X GET -H Content-Type: application/json http://localhost:5000/nodes/resolve
{"message":"Our chain was replaced","new_chain":[{"index":1,"previous_hash":"1","proof":100,"timestamp":1628093932.5106084,"transactions":[]},{"index":2,"previous_hash":"5a976c5fa7492662bf099c5716887f7320bb37ddb012a75906185e8e1c771280","proof":46358,"timestamp":1628093943.819545,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]},{"index":3,"previous_hash":"4b62614dfdc227227b1d4fde56805dfe745c8d18cb8bbc037b526b05f3877281","proof":95587,"timestamp":1628093943.9456918,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]},{"index":4,"previous_hash":"e2e5f0b0c0da7e8a3479967a1587aee601b59c2b7634b772e9b44a9d71ecea7d","proof":140560,"timestamp":1628093944.089444,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]},{"index":5,"previous_hash":"753d498348a2c679a33c72827af7f6c24b9379bdeeb3126ed79c00464736e070","proof":1111,"timestamp":1628093944.0962727,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]}]}

curl -s -X GET -H Content-Type: application/json http://localhost:5000/chain
{"chain":[{"index":1,"previous_hash":"1","proof":100,"timestamp":1628093932.5106084,"transactions":[]},{"index":2,"previous_hash":"5a976c5fa7492662bf099c5716887f7320bb37ddb012a75906185e8e1c771280","proof":46358,"timestamp":1628093943.819545,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]},{"index":3,"previous_hash":"4b62614dfdc227227b1d4fde56805dfe745c8d18cb8bbc037b526b05f3877281","proof":95587,"timestamp":1628093943.9456918,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]},{"index":4,"previous_hash":"e2e5f0b0c0da7e8a3479967a1587aee601b59c2b7634b772e9b44a9d71ecea7d","proof":140560,"timestamp":1628093944.089444,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]},{"index":5,"previous_hash":"753d498348a2c679a33c72827af7f6c24b9379bdeeb3126ed79c00464736e070","proof":1111,"timestamp":1628093944.0962727,"transactions":[{"amount":1,"recipient":"ff2e45ebb4a64c2282f3f1ecd128c629","sender":"0"}]}],"length":5}

Test passed -- the chain was changed.
```
