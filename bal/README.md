##BlockChain Alchemy Laboratory Framework

The goal of the project "Block[Chain] Alchemy Lab" ‒ creation of a toolkit for educational and research activities for blockchain-related tasks. This toolkit consist from programming framework and tools for modelling different blockchain and not only blockchain (indicated by the optionality of "chain") environments. Programming framework oriented to flexibility and possibility of changing different layers of architecture: consensus machinery, storage systems, transaction transport, smart contract implementation, etc.

*Dependencies*
- Docker >= 20.10.7

##### Run Instructions:
There is a MakeFile with basic commands to build and run the image. The commands are:

    make start-quant (TAG, PORT are optional arguments)
This command will build and run the image. Use it when you are running the image for the first time.
The default port is 5002.

    make run-quant
This command run an already built image. Use it when you had already built / run the image
You can run multiple containers by defining a port argument along with the above command. e.g:
    
    make run-quant PORT=5003
    make run-quant PORT=5004

##### Testing:

Log into docker container using:

`docker exec -it $(docker ps | awk '{print $1, $2}' | grep bal:v1 | awk '{print $1}') bash`
